<?php

use yii\db\Migration;

class m170128_113552_product extends Migration
{
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description_image' => $this->string(255),
            'product_type_id' => $this->integer(11)->notNull(),
            'category_id' => $this->integer(11)->notNull()
        ]);
    }

    public function down()
    {
        echo "m170128_113552_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
