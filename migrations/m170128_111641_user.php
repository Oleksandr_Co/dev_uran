<?php

use yii\db\Migration;

class m170128_111641_user extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255),
            'password' => $this->string(255),
        ]);
    }

    public function down()
    {
        echo "m170128_111641_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
