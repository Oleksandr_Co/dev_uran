<?php

use yii\db\Migration;

class m170128_113427_product_type extends Migration
{
    public function up()
    {
        $this->createTable('product_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)
        ]);
    }

    public function down()
    {
        echo "m170128_113427_product_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
