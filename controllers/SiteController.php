<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Login;
use app\models\Signup;
use app\models\Search;
use app\models\PostProduct;
use app\models\NewImage;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /*
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
        ];
    }
    /*
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /*
     *
     * Signup action.
     *
     */
    public function actionSignup()
    {
        $model = new Signup();
        if(isset($_POST['Signup']))
        {
            $model->attributes = Yii::$app->request->post('Signup');
            if($model->validate() && $model->signup())
            { 
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('signup',['model' => $model]);
    }
    
    /*
     * 
     * Login action.
     * 
     */
     public function actionLogin()
    {
        if(!Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }
        $login_model = new Login();
        if( Yii::$app->request->post('Login'))
        {
            $login_model->attributes = Yii::$app->request->post('Login');
            if($login_model->validate())
            {
                Yii::$app->user->login($login_model->getUser());
                return $this->goHome();
            }
        }

        return $this->render('login', ['login_model' => $login_model]);
    }
    /*
     *
     * Logout action.
     *
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    /*
     *
     * Displays index page.
     *
     */   
    public function actionIndex()
    {
        $category_query = new Query;
        $category_query->select('id, name')
                       ->from('category');
        $categories  = $category_query->all();
        $id = Yii::$app->user->getId();
        $product_query = new Query;
        $product_query->select('description_image, id, name')
                     ->from('product');
        $products  = $product_query->all();

    	return $this->render('index', ['products' => $products, 'categories' => $categories]);
    }
     /*
     *
     * Search action.
     *
     */
    public function actionSearch() 
    {
         if(!Yii::$app->user->getId())
        {
           return $this->goHome();  
        }
        $model = new Search();
        if ($model->load(Yii::$app->request->post())&&$model->validate()) {
            $post = $model->searchstring;
            $id   = htmlspecialchars($post);
            return $this->redirect('?r=site%2Fresult&id=' . $id);
        }
    }
    /*
     *
     * Result action.
     *
     */
    public function actionResult() 
    {
        if(!Yii::$app->user->getId())
        {
           return $this->goHome();  
        }

        $id = htmlspecialchars($_GET['id']);
        $new_id = mb_strtolower($id);
        $product_query  = new Query;
        $product_query  ->select('description_image, id, name')
                        ->from('product');
        $products       = $product_query->all();
        $x = count($products);
        $category_products = array();
        for($i=0;$i<$x;$i++){
            $product_name = mb_strtolower($products[$i]['name']);
            $pos = strpos($product_name, $new_id);
            if($pos===false){

            }else{
                array_push($category_products, $products[$i]);
            }
        }

        return $this->render('result', ['category_products' => $category_products]);

    }
    /*
     *
     * Delete product action.
     *
     */
    public function actionDeleteproduct()
    {
        if(!Yii::$app->user->getId())
        {
           return $this->goHome();  
        }

        return $this->renderAjax('deleteproduct');
    }

    public function actionDeleteproductaction()
    {
        if(!Yii::$app->user->getId())
        {
           return $this->goHome();  
        }
        $id    = (int) $_GET['id'];
        $image = htmlspecialchars($_GET['image']);
        \Yii::$app
            ->db
            ->createCommand()
            ->delete('product', ['id' => $id ])
            ->execute();
        unlink(Yii::$app->basePath . '/web/uploads/' . $image);
        Yii::$app->session->setFlash('delete', "Post new");

        return $this->redirect('?r=site%2Findex');
    }
    /*
     *
     * Postproduct action.
     *
     */
    public function actionPostproduct()
    {
        if(!Yii::$app->user->getId())
        { 
            return $this->goHome();  
        }

        $category_query = new Query;
        $category_query ->select('id, name')
                        ->from('category');
        $categories     = $category_query->all();

        $product_type_query = new Query;
        $product_type_query ->select('id, name')
                            ->from('product_type');
        $product_types      = $product_type_query->all();

        $model = new PostProduct();
        if (Yii::$app->request->isPost) 
        {

            $model->attributes = Yii::$app->request->post('PostProduct');
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->validate() && $model->upload()) 
            {      
                \Yii::$app->db->createCommand()
                    ->insert('product', [
                        'name'              => $model->name,
                        'product_type_id'   => $model->product_type,
                        'category_id'       => $model->category,
                        'description_image' => $model->imageFile->baseName . time() . '.' . $model->imageFile->extension,
                    ])->execute();
                Yii::$app->session->setFlash('success', "All ok");

                return Yii::$app->getResponse()->redirect('?r=site%2Findex');
            } else {
                Yii::$app->session->setFlash('danger', "Image must be png, jpg, jpeg extensions and less than 2MB");

                return Yii::$app->getResponse()->redirect('?r=site%2Findex');
            }
        }

        return $this->render('postproduct', ['model' => $model, 'categories' => $categories, 'product_types' => $product_types]);
    }    
   /*
     *
     * Newimage action.
     *
     */
     public function actionNewimage()
    {
        if(!Yii::$app->user->getId())
        { 
            return $this->goHome();  
        }
        $id    = (int) $_GET['id'];
        $model = new NewImage();
        if (Yii::$app->request->isPost) 
        {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');


            if ($model->validate() && $model->upload()) 
            {      
                \Yii::$app->db->createCommand("UPDATE product SET   description_image=:new_image WHERE id=:id")
                            ->bindValue(':id',$id)
                            ->bindValue(':new_image', $model->imageFile->baseName . time() . '.' . $model->imageFile->extension)
                            ->execute();
                Yii::$app->session->setFlash('success', "All ok");

                return Yii::$app->getResponse()->redirect('?r=site%2Findex');
            } else {
                Yii::$app->session->setFlash('danger', "Image must be png, jpg, jpeg extensions and less than 2MB");

                return Yii::$app->getResponse()->redirect('?r=site%2Findex');
            }
        }

        return $this->renderAjax('newimage', ['model' => $model]);
    }   
    /*
     *
     * Category action.
     *
     */
    public function actionCategory() 
    {
        $name = htmlspecialchars($_GET['name']);
        $categories_query  = new Query;
        $categories_query->select('id, name')
                         ->from('category');
        $categories        = $categories_query->all();
        $category_query    = new Query;
        $category_query->select('id')
                       ->from('category')
                       ->where(['name'=>$name]);
        $index_category = $category_query->one();
        $products_query = new Query;
        $products_query->select('description_image, id, name')
                        ->from('product')
                        ->where(['category_id'=>$index_category]);
        $products  = $products_query->all();
        return $this->render('category', ['products' => $products, 'categories' => $categories, 'name' => $name]);
    } 
}
