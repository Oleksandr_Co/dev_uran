/*
* jquery for modal windows
*/
$(function() {
	$('.deleteimageButton').click(function() {
		$('#show').modal('show')
			.find('#deleteimageContent')
			.load($(this).attr('value'));
	});
	$('.postimageButton').click(function() {
		$('#postimage').modal('show')
			.find('#postimageContent')
			.load($(this).attr('value'));
	});
});



