<?php
namespace app\models;
use yii\base\Model;
use yii\web\UploadedFile;

class PostProduct extends Model
{

    public $name;
    public $imageFile;
    public $category;
    public $product_type;

    public function rules()
    {
        return [
            [['name'], 'string', 'min' => 3,'max' => 120],
            [['name','category','product_type'], 'required'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 1024 * 1024 * 2],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $image = $this->imageFile->baseName . time() . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . time() . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}