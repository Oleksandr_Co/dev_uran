<?php
namespace app\models;
use yii\base\Model;
use yii\web\UploadedFile;

class NewImage extends Model
{

    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 1024 * 1024 * 2],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $image = $this->imageFile->baseName . time() . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . time() . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}