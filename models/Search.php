<?php

namespace app\models;
use Yii;
use yii\base\Model;


class Search extends Model
{
   
    public $searchstring;
    
   

    public function rules()
    {
        return [
            [['searchstring'],'required'],
            ['searchstring','string','min'=>3,'max'=>50],
        ];
    }
}