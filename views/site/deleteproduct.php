<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="text-center">
	<?php echo "<h3>Delete This Product?</h3>"; ?>
	<a href="<?= Url::to('index.php')?>">
		<div>
      	<p class="btn btn-info">No</p> 
    </div>
  </a><br>
  <a href="<?= Url::to('index.php?r=site%2Fdeleteproductaction&id=' . $_GET['id'] . '&image=' . $_GET['image'])?>">
    <div>
      	<p class="btn btn-danger">Yes</p> 
    </div>
  </a>
</div>