<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<h4 class="text-center">Signup</h4>
<?php
    $form = ActiveForm::begin(['class'=>'form-horizontal']);
?>

<?= $form->field($model,'email')->textInput(['autofocus'=>true]) ?>

<?= $form->field($model,'password')->passwordInput() ?>

<div>
    <button type="submit" class="btn btn-primary">Submit</button>
</div>

<?php
    $form = ActiveForm::end();
?>