<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
?>
	<?php	$x = count($category_products);?>
	<?php if(!$x): ?>
		<h3>Sorry Did not find. Try Again!</h3>
	<?php endif ;?>

	<?php for($i = 0; $i<$x;$i++):?> 
		<div class="col-md-4">
            <div class="thumbnail">
                <div class="image">
                	<h4 class="text-center"><?= $category_products[$i]['name']?></h4>
                    <?= Html::img('uploads/' . $category_products[$i]['description_image'],['alt' => 'Sorry This Product Do not', 'class' => 'image', 'position' => 'absolute', 'height' => '270px', 'width' => '340px']) ?>
                </div>
                <br>
                <div class="text-center">
                    <?= Html::button('Post New Image', ['value' => Url::to('index.php?r=site%2Fnewimage&id=' . $category_products[$i]['id']),'class' => 'postimageButton btn btn-info']) ?> 
                    <?php 
                         Modal::begin([
                            'size' => 'modal-lg',
                            'id'   => 'postimage',
                         ]);
                        echo "<div id='postimageContent'></div>";

                        Modal::end();
                    ?>
                                
                    <?= Html::button('Delete This Product', ['value' => Url::to('index.php?r=site%2Fdeleteproduct&id=' . $category_products[$i]['id'] . '&image=' . $category_products[$i]['description_image']), 'class' => 'deleteimageButton btn btn-danger']) ?>
                    <?php 
                        Modal::begin([
                            'size' => 'modal-lg', 
                            'id'   => 'show',
                        ]);
                        echo "<div id='deleteimageContent'></div>";

                        Modal::end();
                    ?> 
                </div>     
            </div>
        </div>
    <?php endfor;?>