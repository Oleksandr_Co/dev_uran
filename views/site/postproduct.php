<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php        
    $x = count($categories);
    $y = count($product_types);

?>
<?php 
	$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) 
?>

	<?= $form->field($model,'name')->textInput(['placeholder' => 'Product Name']) ?>
	<?php
		
		$new2 = array('0'=>'unset');
		
		for($i=0; $i<$x;$i++){
			array_push($new2,$categories[$i]['name']);
		}
		unset($new2[0]);
    	
	    $params = [
	        'prompt' => 'Choose category...'
	    ];
	    echo $form->field($model,'category')->dropDownList($new2,$params);
      
	?>
	<?php
		
		$new = array('0'=>'unset');
		
		for($i=0; $i<$y;$i++){
			array_push($new,$product_types[$i]['name']);
		}
		unset($new[0]);
    	
	    $params = [
	        'prompt' => 'Choose product Type...'
	    ];
	    echo $form->field($model,'product_type')->dropDownList($new,$params);
      
	?>
	

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <button class="btn btn-danger">Submit</button>

<?php 
	$form = ActiveForm::end() 
?>

