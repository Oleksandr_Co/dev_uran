<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php 
	$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) 
?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <button class="btn btn-danger">Submit</button>

<?php 
	$form = ActiveForm::end() 
?>



