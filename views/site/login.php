<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<h4 class="text-center">Login in</h4>
<?php 
	$form = ActiveForm::begin();
?>

<?= $form->field($login_model,'email')->textInput() ?>

<?= $form->field($login_model,'password')->passwordInput() ?>

<div>
    <button class="btn btn-success" type="submit">Login</button>
</div>

<?php 
	$form = ActiveForm::end(); 
?>

 
