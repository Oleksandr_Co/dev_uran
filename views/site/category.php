<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
?>
<?php $this->registerJsFile('web/js/main'); ?>
<?php $this->registerCssFile('web/css/index'); ?>
<div class="container">
    <?php if (!(Yii::$app->user->getId())): ?>
        <div>
            <h4>You must login to do what you want!</h4>
        </div>
    <?php endif;?>
    <?php if (Yii::$app->user->getId()): ?>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td>
                        <?php 
                            $x = count($categories);
                        ?>
                        <div class="row">
                            <?php for($i = 0; $i<$x;$i++):?>
                                    <div class="col-sm-4" style="background-color:;">
                                        <a href="index.php?r=site%2Fcategory&name=<?php echo $categories[$i]['name'];?>"><h4 style="margin:10px;<?php if($categories[$i]['name'] === $name): ?>background-color:green;<?php else: ?>background-color:#008ae6;<?php endif; ?>color:white;padding:20px;text-align:center;text-align:center;"><?php echo $categories[$i]["name"];?></h4></a>
                                    </div>
                            <?php endfor;?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php if(count($products) > 0): ?>
                <?php for($i = 0; $i < count($products); $i++): ?>
             	<h4 class="text-center"></h4>
                    <div class="col-md-4">
                        <div class="thumbnail">
                                <div class="image">
                                <h4 class="text-center"><?= $products[$i]['name']?></h4>
                                <?= Html::img('uploads/' . $products[$i]['description_image'],['alt' => 'Sorry This Product Do not', 'class' => 'image', 'position' => 'absolute', 'height' => '270px', 'width' => '340px']) ?>
                                </div>
                                <br>
                                <div class="text-center">
                                    <?= Html::button('Post New Image', ['value' => Url::to('index.php?r=site%2Fnewimage&id=' . $products[$i]['id']),'class' => 'postimageButton btn btn-info']) ?> 
                                     <?php 
                                        Modal::begin([
                                            'size' => 'modal-lg',
                                            'id'   => 'postimage',
                                        ]);
                                        echo "<div id='postimageContent'></div>";

                                        Modal::end();
                                    ?>
                                
                                    <?= Html::button('Delete This Product', ['value' => Url::to('index.php?r=site%2Fdeleteproduct&id=' . $products[$i]['id'] . '&image=' . $products[$i]['description_image']), 'class' => 'deleteimageButton btn btn-danger']) ?>
                                        <?php 
                                            Modal::begin([
                                                'size' => 'modal-lg', 
                                                'id'   => 'show',
                                            ]);
                                            echo "<div id='deleteimageContent'></div>";

                                            Modal::end();
                                        ?> 
                                </div>     
                        </div>
                    </div>
                      
                <?php endfor; ?>

        <?php endif; ?>
        <?php if(count($products) == 0): ?>
            <h4>Sorry This Catogory now empty</h4>
        <?php endif; ?>    
    <?php endif; ?> 
</div>

