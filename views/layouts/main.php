<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use app\models\Search;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Task',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Main page', 'url' => ['/site/index']],
            Yii::$app->user->isGuest ? (
                ['label' =>'Login',
                    'items'=>[
                        [
                        'label'=>'Login','url' => ['/site/login']
                        ],
                        [
                        'label'=>'SignUp','url' => ['/site/signup']
                        ]
                    ]
                ]
            ) : (
            ['label' =>'Cabinet',
                'items'=>[
                    [
                    'label'=>'Post Product','url' => ['/site/postproduct']
                    ],
                    [
                    'label'=>'Logout','url' => ['/site/logout']
                    ]
                ]
            ]
            )
        ],
    ]);
        $model = New Search();
        $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['site/search']),]);
            echo $form->field($model, 'searchstring', [
                'template' => '<div class="input-group col-xs-3  navbar-right" style="margin-right:20px; margin-top:-7px;">{input}<span class="input-group-btn">' .
                        Html::submitButton('GO', ['class' => 'btn btn-info']) . '</span></div>',
                ])->textInput(['placeholder' => 'Search']);  
        $form = ActiveForm::end();
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <? ?></p>

        <p class="pull-right"><?echo "This new project by Alex";?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
